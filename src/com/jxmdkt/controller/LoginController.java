package com.jxmdkt.controller;

import com.jfinal.aop.ClearInterceptor;
import com.jfinal.core.Controller;
import com.jxmdkt.common.MD5;
import com.jxmdkt.model.AdminModel;
import com.jxmdkt.model.Constants;

@ClearInterceptor
public class LoginController extends Controller{
	public void index(){
		render("login.jsp");
	}
	public void relogin(){
		if(getSessionAttr("loginedadmin")!=null)
			getSession().removeAttribute("loginedadmin");
		render("login.jsp");
	}
	public void login(){
		String adminid=getPara("adminid");
		String password=getPara("password");
		Object objec=AdminModel.dao.login(adminid, password);
		if(objec instanceof AdminModel){
			setSessionAttr("loginedadmin", objec);
			if(((AdminModel) objec).getStr("password").equals(MD5.MD5(Constants.DEFAULT_PASSWORD))){
				String message="你当前的密码为初始密码，请修改密码！";
				setAttr("errormessage", message);
				setAttr("firstchange", "true");
				render("/admin/cp.jsp");
			}else
				renderJsp("/admin/indexMain.jsp");
		}else if (objec instanceof String) {
			setAttr("errormsg", objec);
			forwardAction("/");
		}
	}
}
