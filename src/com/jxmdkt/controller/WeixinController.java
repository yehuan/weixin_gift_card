package com.jxmdkt.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.aop.ClearInterceptor;
import com.jfinal.core.Controller;
import com.jfinal.plugin.ehcache.CacheName;
import com.jxmdkt.model.UinfoModel;

@ClearInterceptor
public class WeixinController extends Controller{
	public void index(){
		render("/weixin.jsp");
	}
	public void toquery(){
		render("/weixinquery.jsp");
	}
	
	public void query(){
		String cardno=getPara("cardno");
		String phoneno=getPara("phoneno");
		Object o=UinfoModel.dao.query(cardno, phoneno);
		if(!(o instanceof String)){
			UinfoModel model=(UinfoModel)o;
			String delivered=model.getStr("delivered");
			if("1".equals(delivered)){
				setAttr("delivercp", model.getStr("delivercp"));
				setAttr("deliverno", model.getStr("deliverno"));
				render("/weixinquerysuccess.jsp");
			}else {
				render("/weixinquerynone.jsp");
			}
		}else{
			render("/weixinerrorresult.jsp");
		}
		
	}
	
	public void submit(){

		UinfoModel model=new UinfoModel();
		model.set("cardno", getPara("cardno"));
		model.set("password", getPara("password"));
		model.set("name", getPara("name"));
		model.set("phoneno", getPara("phoneno"));
		model.set("comment", getPara("comment"));
		Date date=new Date();
		String datetime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		model.set("datetime", datetime);
		String prov=getPara("seachprov");
		String address= prov+getPara("detail");
		model.set("address",address);
		String result=UinfoModel.dao.insert(model);
		if(result==null){
			renderJsp("/weixinsuccessresult.jsp");
		}else {
			if("used".equals(result))
				renderJsp("/weixincardused.jsp");
			else
				renderJsp("/weixinerrorresult.jsp");
		}
	}
	
}
