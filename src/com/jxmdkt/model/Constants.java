package com.jxmdkt.model;

public final class Constants
{
	public static final String PERMISSION_ADMIN = "0";
	public static final String PERMISSION_EXPORT = "1";
	public static final String PERMISSION_IMPORT = "2";
	public static final String PERMISSION_QUERY = "3";
	public static final String PERMISSION_DELIVER = "4";
	public static final String ADMINSTATUS_HIDDEN = "0";
	public static final String ADMINSTATUS_LUNCHED = "1";
	public static final String DEFAULT_PASSWORD = "123456";
	public static final String DELIVERED_TRUE = "1";
	public static final String DELIVERED_FALSE = "0";
}
