 package com.jxmdkt.model;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

 public class AdminModel extends Model<AdminModel>
 {
 public static final AdminModel dao = new AdminModel();
 
   public Object login(String adminid, String password) { 
	   AdminModel model = (AdminModel)findById(adminid);
     if (model == null)
       return "用户名错误，请重新输入！";
     if ("0".equals(model.getStr("status")))
       return "您的账户已经被禁用！";
     if (!password.equals(model.getStr("password")))
       return "密码错误，请重新输入！";
     return model;
     }
 
   public Page<AdminModel> paginate(int pagesize, int pagenow)
   {
     return paginate(pagenow, pagesize, "select *  ", "from admin_t");
   }
   public String create(AdminModel model) {
     AdminModel mo = (AdminModel)findById(model.getStr("id"));
     if (mo != null)
       return "账号" + model.getStr("id") + "已存在！";
     model.save();
     return null;
   }
 
   public boolean changestatus(String adminid, String status) {
     try {
       AdminModel model = (AdminModel)findById(adminid);
       ((AdminModel)model.set("status", status)).update();
     } catch (Exception e) {
       e.printStackTrace();
       return false;
     }
     return true;
   }
 }

