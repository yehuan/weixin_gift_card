package com.jxmdkt.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.jxmdkt.model.AdminModel;

public class CheckStaticHandler extends Handler{
	
	private String viewpostfix=".jsp";

	@Override
	public void handle(String target, HttpServletRequest request,
			HttpServletResponse response, boolean[] isHandled) {
				int index=request.getRequestURL().lastIndexOf(viewpostfix);
				System.out.println(request.getRequestURI());
				AdminModel admin=(AdminModel) request.getAttribute("loginedadmin");
				if(admin==null)
					if(index!=-1)
						target="/";
			nextHandler.handle(target, request, response, isHandled);
	}

}
