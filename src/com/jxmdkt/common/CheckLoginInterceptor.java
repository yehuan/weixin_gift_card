package com.jxmdkt.common;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jxmdkt.model.AdminModel;

public class CheckLoginInterceptor  implements Interceptor{

	public void intercept(ActionInvocation ai) {
		Controller controller=ai.getController();
		AdminModel model=controller.getSessionAttr("loginedadmin");
		if(model==null){
	
			controller.render("/loginerror.jsp");
		}
		else
			ai.invoke();
	}

}
