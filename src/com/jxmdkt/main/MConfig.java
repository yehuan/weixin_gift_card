package com.jxmdkt.main;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.log.Log4jLoggerFactory;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jxmdkt.common.CheckLoginInterceptor;
import com.jxmdkt.controller.AdminController;
import com.jxmdkt.controller.LoginController;
import com.jxmdkt.controller.WeixinController;
import com.jxmdkt.model.AdminModel;
import com.jxmdkt.model.UinfoModel;

/**
 * API引导式配置
 */
public class MConfig extends JFinalConfig {
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用getProperty(...)获取值
		loadPropertyFile("a_little_config.txt");
		me.setDevMode(getPropertyToBoolean("devMode", true));
		me.setVelocityViewExtension(".jsp");
	
		me.setLoggerFactory(new Log4jLoggerFactory());
		me.setMaxPostSize(1024*1024*1000);
		
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add("/", LoginController.class);
		me.add("/admin", AdminController.class);
		me.add("/weixin",WeixinController.class);
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		
		C3p0Plugin cpp=new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password"));
		me.add(cpp);
		ActiveRecordPlugin arp =new ActiveRecordPlugin(cpp);
		arp.setDialect(new MysqlDialect());
		arp.setTransactionLevel(4);
		arp.addMapping("uinfo_t","cardno", UinfoModel.class);
		arp.addMapping("admin_t", "id", AdminModel.class);
		
		me.add(arp);
	}
	
	/** 
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.add(new CheckLoginInterceptor());
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
	}
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		
		JFinal.start("WebRoot", 80, "/", 5);
		
	}
}
