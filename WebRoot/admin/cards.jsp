

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Author:xxf">
		<link rel="STYLESHEET" type="text/css" href="css/style.css">
		<link rel="STYLESHEET" type="text/css" href="css/admin.css">
		<script language="javascript" src="/js/calendar/WdatePicker.js"></script>
		 <script language="JavaScript">
		 		
			function query(){
				document.forms["myform"].action="/admin/query";
	 			document.forms["myform"].submit();
			}
			
			
			
			
			function checkdate(){
			
				if(document.myform.con_dateStart.value>document.myform.con_dateEnd.value){
				    return false;
				}else 
					return true;
			}
				
			function exportAll(){
				document.forms["myform"].action="/admin/exportAll";
	 			document.forms["myform"].submit();
			}
			function exportNowPage(){
				document.forms["myform"].action="/admin/exportNowPage";
	 			document.forms["myform"].submit();
			}

     function checkpageNo(id){
	   var regex = "^[0-9_]{0,}$";
	   if (id.match(regex) == null){
			alert("输入的页码只允许数字");
			return false;
	   }     
	   return true; 
    }
    function checkall(){

    	var obj = document.getElementsByName('selectall');
    	
    		obj[0].checked=false;
    	
    }
    function selectAll() {
 		var obj = document.getElementsByName('selectall');
 		var cks = document.getElementsByTagName("input");
 		var ckslen = cks.length;
 		for(var i=0;i<ckslen-1;i++) {
  			if(cks[i].type == 'checkbox') {
   				cks[i].checked = obj[0].checked;
 		 	}
 		}
	}
	function deliver(){
		var cardnos="";
		var cks=document.getElementsByTagName("input");
		for(var i=0;i<cks.length;i++){
			if(cks[i].type=='checkbox'&&cks[i].name!='selectall'&&cks[i].checked){
				var cardno=cks[i].value;
				cardnos+=cardno;
				cardnos+=";";
			}
		}
		if(cardnos==""){
			alert("请选择要发货的礼品");
			return false;
		}
		document.forms["pageform"].action="/admin/deliver?cardnos="+cardnos;
		document.forms["pageform"].submit();
	}
	function goSearch(pagenumber,pagecount)
	{   
		var pageno=document.forms["pageform"].pageNumber.value;
		if (pageno != null){
			if(pagenumber==pageno){
				return false;
			}
				
			if (pageno >0){
	            if (pageno > pagecount)         
			       document.getElementById('pageNumber').value=pagecount;
	           	
			}else if (pageno <1){
			    document.getElementById('pageNumber').value=1;
			}
		 if(!checkpageNo(pageno)){ 
          	return false;
       	 }     	 
	  }
	}
			
	</script>
	</head>

	<body>

		<form id="myform" name="myform" style="width:100%;">
		
			<table style="width:100%;align: left;">
				<tr>
				<td style="width: 100%">
				</br></br>&nbsp;&nbsp;&nbsp;
					<span class="title">查询条件</span>
					<br>
					<!-- <span class="remark">说明：本页显示用户提交的信息</span> -->
				</td>
				</tr>
				<tr>
					<td>
						<table style="width:100%">
							<tr>
								<td align="right" class="loginfontstyle" style="font-size:14px;" >
									
									&nbsp;&nbsp;&nbsp;&nbsp;
									
									<span class="keystone">起始日期:</span>
									<input   name="starttime" onClick="new WdatePicker(this,'%Y-%M-%D',false)" value="${(starttime)!}" size="10" /> 
									<span class="keystone">截止日期:</span>
									<input  name="endtime" onClick="new WdatePicker(this,'%Y-%M-%D',false)" value="${(endtime)!}" size="10" /> 
									
									
									<select name="querycondition"    size="1">
									<option value="0" selected>==请选择==</option>
									<#if querycondition=="1" >
										<option value="1" selected >已使用卡号</option>
									<#else>
										<option value="1"  >已使用卡号</option>
									</#if>
									<#if querycondition == "2" >
									<option value="2" selected >未使用卡号</option>
									<#else>
									<option value="2"  >未使用卡号</option>
									</#if>
									</select>
									<select name="deliveredcondition"    size="1">
									<option value="0" selected>==请选择==</option>
									<#if deliveredcondition == "1" >
										<option value="1" selected >已发货</option>
									<#else>
										<option value="1"  >已发货</option>
									</#if>
									<#if deliveredcondition == "2" >
									<option value="2" selected >未发货</option>
									<#else>
									<option value="2"  >未发货</option>
									</#if>
									</select>
									<input type="hidden" id="pagecount" name="pagecount" value="${page.totalRow!}" />
									&nbsp;&nbsp;<input type="button" class="buttonstyle" value="查询" onclick="query()">
									<!-- &nbsp;&nbsp;<input type="button" class="buttonstyle"  value="导出本页" onclick="exportNowPage()"> -->
									<#if permission.equals("1")>
									&nbsp;&nbsp;<input type="button" class="buttonstyle"  value="导出" onclick="exportAll()">
									</#if>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="30px">
				</tr>
				<tr>
					<td  background="images/hr_1.gif"
						style="height: 1px; width: 100%">
						<div class="hr1">
						</div>
					</td>
				</tr>
				<tr>
				
				<td style="width: 100%">
					</br>
					<span class="title">礼品卡信息表</span></br></br>
					<#if permission.equals("4") ><input id="deliverbutton" name="deliverbutton" type="button" class="buttonstyle" value="发货" onclick="return deliver();"></#if>
					<br>
				</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="3" cellspacing="0"
							bordercolor="#AAAAAA" border="1"
							style="border-collapse: collapse" align="left">
							<tr BGCOLOR="#1E90FF" align="center" style="height: 30px">
							<#if permission.equals("4")>
								<td class="formtitlestyle" style="width: 5%">
									<input type="checkbox" name="selectall" onclick="selectAll()" >全选
								</td>
							</#if>
								<td class="formtitlestyle" style="width: 10%">
									<b>卡号</b>
								</td>
								<td class="formtitlestyle" style="width: 10%">
									<b>收货人</b>
								</td>
								<td class="formtitlestyle" style="width: 25%">
									<b>地址</b>
								</td>
								<td class="formtitlestyle" style="width: 10%">
									<b>发货状态</b>
								</td>
								<td class="formtitlestyle" style="width: 10%">
									<b>手机</b>
								</td>
								<td class="formtitlestyle" style="width: 10%">
									<b>属地</b>
								</td>
								<td class="formtitlestyle" style="width: 10%">
									<b>录入日期</b>
								</td>
								<td class="formtitlestyle" style="width: 15%">
									<b>备注</b>
								</td>
							</tr>
								<#list page.getList() as card >
								<tr style="height: 25px">
									<#if permission.equals("4")>
										<td class="loginfontstyle" style="width: 5%" align="center">
										<#if card.delivered.equals("0")&&card.used.equals("是") >
											
												<input type="checkbox" name="${card.cardno }" onclick="checkall()" value="${card.cardno}" />
											</#if>
											
										</td>
									</#if>
									<td class="loginfontstyle" style="width: 10%" align="center">
										${card.cardno}
									</td>
									<td class="loginfontstyle" style="width: 10%" align="center">
										${card.name}
									</td>
									<td class="loginfontstyle" style="width: 25%" align="center">
										${card.address}
									</td>
									<td class="loginfontstyle" style="width: 10%" align="center">
										<#if card.delivered.equals("1") >
											已发货
										<#else>
											未发货
										</#if>
									</td>
									<td class="loginfontstyle" style="width: 10%" align="center">
										${card.phoneno}
									</td>
									<td class="loginfontstyle" style="width: 10%" align="center">
										${(card.cardcomment)!}
									</td>
									
									<td class="loginfontstyle" style="width: 10%" align="center">
										${card.datetime}
									</td>
									
									<td class="loginfontstyle" style="width: 15%" align="center">
										${card.comment}
									</td>
								</tr>
							</#list>
						</table>
					</td>
				</tr>
			</table>
		</form>
		
		<form id="pageform" name="pageform" method="post"  action="/admin/query" onsubmit=" return goSearch(${page.pageNumber! },${page.totalPage!});">
			<table width="100%" border="0" align="center" cellpadding="0"
			cellspacing="0">
			<tr style="color: black;">
				<td height="30" align="center">
					共
					<strong>${page.totalRow!}
					</strong> 条&nbsp;&nbsp;
					<#if page.getPageNumber() gt 1 >
						<a style="cursor: hand"	href="/admin/query?starttime=${starttime! }&endtime=${endtime! }&pageNumber=1&querycondition=${querycondition!}&deliveredcondition=${deliveredcondition!}"><font color="red"><b>[首页]</b></font></a>
						<a style="cursor: hand"	href="/admin/query?starttime=${starttime! }&endtime=${endtime! }&pageNumber=${page.pageNumber-1 }&querycondition=${querycondition!}&deliveredcondition=${deliveredcondition!}"><font color="red"><b>[上一页]</b></font></a>
					<#else>
						<b>[首页]</b>
						<b>[上一页]</b>
					</#if>
					
					<#if page.getPageNumber() lt page.getTotalPage() >
						<a style="cursor: hand"	href="/admin/query?starttime=${starttime! }&endtime=${endtime! }&pageNumber=${page.pageNumber+1}&querycondition=${querycondition!}&deliveredcondition=${deliveredcondition!}"><font color="red"><b>[下一页]</b></font></a>
						<a style="cursor: hand" href="/admin/query?starttime=${starttime! }&endtime=${endtime! }&pageNumber=${page.totalPage!}&querycondition=${querycondition!}&deliveredcondition=${deliveredcondition!}"><font color="red"><b>[尾页]</b></font></a>
					<#else>
						<b>[下一页]</b>
						<b>[尾页]</b>
					</#if>
					
					&nbsp;&nbsp;页次：
					${page.pageNumber!}
					/
					<#if page.totalPage==0>
						1
					<#else>
					${page.totalPage!}
					</#if>
					页
					<strong>${page.pageSize!}
					</strong>条/页 转到：第
						<input type="text" size="2" id="pageNumber" name="pageNumber"  value="${(page.pageNumber)!}" class="tfield7" />
						页
						<input type="submit" class="buttonstyle" value="go!"  />
						<input type="hidden"  name="pagenoo"  value="${page.pageNumber!}" />
						<input type="hidden"  name="querycondition"  value="${querycondition!}" />
						<input type="hidden"  name="deliveredcondition" value="${deliveredcondition!}" />
						<input type="hidden"  name="starttime" value="${starttime!}" />
						<input type="hidden" name="endtime" value="${endtime!}" />
						
				</td>
			</tr>
		</table>
	</form>
	</body>
</html>
