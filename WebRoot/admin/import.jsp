
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Author:xxf">
		<link rel="STYLESHEET" type="text/css" href="css/style.css">
		<link rel="STYLESHEET" type="text/css" href="css/admin.css">
		<script type="text/javascript" src="http://lib.sinaapp.com/js/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="/js/jquery_upload.js"></script>
		<script language="JavaScript">
		function doImport() {
				if(document.myform.fileUpload.value==""){
					alert("请选择要导入的文件");
					return false;
				}
				document.myform.submitbutton.disabled="true";
				document.myform.submitbutton.value="正在导入...";
		}
		
		function deliverImport() {
			if(document.mydeliverform.deliverfile.value==""){
				alert("请选择要导入的文件");
				return false;
			}
			document.mydeliverform.deliversubmitbutton.disabled="true";
			document.mydeliverform.deliversubmitbutton.value="正在导入...";
	}
		
	</script>
		 
	</head>

	<body>

		<form id="myform" name="myform" style="width:100%;" action="/admin/importer" onsubmit="return doImport();" method="post" enctype="multipart/form-data" >
		
			<table style="width:100%;align: left;">
				<tr>
				<td style="width: 100%">
				</br></br>&nbsp;&nbsp;&nbsp;
					<span class="title">导入礼品卡基本信息</span>
					<br>
				</td>
				</tr>
				<tr height="60px">
					<td>
						<table style="width:100%">
							<tr>
					<td  background="images/hr_1.gif"
						style="height: 1px; width: 100%">
						<div class="hr1">
						</div>
					</td>
				</tr>
							<tr height="50px">
								<td align="left" class="loginfontstyle" style="font-size:14px;"  >
									&nbsp;&nbsp;
									<span class="keystone">请选择文件:</span>
									<input type="file" title="选择文件" name="fileUpload"   id="fileUpload" style="width: 323px; height: 23px"/>  
            						<input  type="submit" id="submitbutton" name="submitbutton"  class="buttonstyle"  value="点击导入"/>&nbsp;&nbsp;${importrst!}
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
				
			</table>

		</form>
		<br><br>
		<form id="mydeliverform" name="mydeliverform" style="width:100%;" action="/admin/deliverexcel" onsubmit="return deliverImport();" method="post" enctype="multipart/form-data" >
		
			<table style="width:100%;align: left;">
				<tr>
				<td style="width: 100%">
				&nbsp;&nbsp;&nbsp;
					<span class="title">导入礼品卡发货信息</span>
					<br>
				</td>
				</tr>
				<tr height="60px">
					<td>
						<table style="width:100%">
							<tr>
					<td  background="images/hr_1.gif"
						style="height: 1px; width: 100%">
						<div class="hr1">
						</div>
					</td>
				</tr>
							<tr height="50px">
								<td align="left" class="loginfontstyle" style="font-size:14px;"  >
									&nbsp;&nbsp;
									<span class="keystone">请选择文件:</span>
									<input type="file" title="选择文件" name="deliverfile"   id="deliverfile" style="width: 323px; height: 23px"/>  
            						<input  type="submit" id="deliversubmitbutton" name="deliversubmitbutton"  class="buttonstyle"  value="点击导入"/>&nbsp;&nbsp;${deliverrst!}
								</td>
							</tr>
							
						</table>
					</td>
				</tr>
				
			</table>

		</form>
	</body>
</html>

