<%@ page language="java" pageEncoding="UTF-8"%>
<head>
<link rel="STYLESHEET" type="text/css" href="css/admin.css">
	<script language="javaScript">
     function checkpageNo(id){
	   var regex = "^[0-9_]{0,}$";
	   if (id.match(regex) == null){
			alert("输入的页码只允许数字");
			return false;
			
	   }     
	   return true; 
    }
	function goSearch(pageno,pagecount)
	{   
		if (pageno != null){
			if (pageno >0){
	            if (pageno > pagecount)         
			       document.getElementById('pageno').value=pagecount;
	            else 	
			       document.getElementById('pageno').value=pageno;	
			}else if (pageno == -1){
			    document.getElementById('pageno').value=1;
			}
			else if (pageno <= -2){
				return;
			}

		if(checkpageNo(pageno)){
           var d = document.forms[0];
           d.action = "${param.ActionName}";
           d.submit();   
        } 		

			
	  }
	}
	
	</script>
</head>
<s:hidden id="pageno" name="pageInfo.pageNo" />
<s:set name="pageno" value="pageInfo.pageNo" />

<s:if test="pageInfo.pageSize>0 && pageInfo.pageNo>0">
	<s:set name="pagepre" value="-2" />
	<s:set name="pagefirst" value="-2" />
	<s:set name="pagenext" value="-2" />
	<s:set name="pagelast" value="-2" />
	<s:if test="pageInfo.pageNo > 1">
		<s:set name="pagepre" value="pageInfo.pageNo-1" />
		<s:set name="pagefirst" value="1" />
	</s:if>
	<s:if test="pageInfo.pageNo < pageInfo.pageCount">
		<s:set name="pagenext" value="pageInfo.pageNo+1" />
		<s:set name="pagelast" value="pageInfo.pageCount" />
	</s:if>
	<table width="90%" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr style="color: black;">
			<td height="30" align="center">
				共
				<strong><s:property value="pageInfo.recordCount" />
				</strong> 条&nbsp;&nbsp;
				<s:if test="pageInfo.pageNo>1">
					<a style="cursor: hand"	onclick="goSearch('${pagefirst}', <s:property value="pageInfo.pageCount"/>)"><font color="red"><b>[首页]</b></font></a>
					<a style="cursor: hand"	onclick="goSearch('${pagepre}', <s:property value="pageInfo.pageCount"/>)"><font color="red"><b>[上一页]</b></font></a>
				</s:if>
				<s:else>
					<b>[首页]</b>
					<b>[上一页]</b>
				</s:else>
				
				<s:if test="pageInfo.pageNo<pageInfo.pageCount">
					<a style="cursor: hand"	onclick="goSearch('${pagenext}', <s:property value="pageInfo.pageCount"/>)"><font color="red"><b>[下一页]</b></font></a>
					<a style="cursor: hand"	onclick="goSearch('${pagelast}', <s:property value="pageInfo.pageCount"/>)"><font color="red"><b>[尾页]</b></font></a>
				</s:if>
				<s:else>
					<b>[下一页]</b>
					<b>[尾页]</b>
				</s:else>
				
				&nbsp;&nbsp;页次：
				<s:property value="pageInfo.pageNo" />
				/
				<s:property value="pageInfo.pageCount" />
				页
				<strong><s:property value="pageInfo.pageSize" />
				</strong>条/页 转到：第
				<input type="text" size="2" name="pagenoReSet" value="${pageno}" class="tfield7" />
				页
				<input type="button" value="go"
					onclick="goSearch(document.forms(0).pagenoReSet.value, <s:property value="pageInfo.pageCount"/>)" />
			</td>
		</tr>
	</table>
</s:if>

