

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="Author:xxf">
		<link rel="STYLESHEET" type="text/css" href="css/style.css">
		<link rel="STYLESHEET" type="text/css" href="css/admin.css">
		 <script language="JavaScript" src="js/calendar/WdatePicker.js"></script>
		 <script language="JavaScript">
		 		
			function query(){
				document.forms["myform"].action="/admin/query";
	 			document.forms["myform"].submit();
			}
			
			
			
			function checkdate(){
			
				if(document.myform.con_dateStart.value>document.myform.con_dateEnd.value){
				    return false;
				}else 
					return true;
			}
				
			function exportAll(){
				document.forms["myform"].action="/admin/exportAll";
	 			document.forms["myform"].submit();
			}
			function exportNowPage(){
				document.forms["myform"].action="/admin/exportNowPage";
	 			document.forms["myform"].submit();
			}

     function checkpageNo(id){
	   var regex = "^[0-9_]{0,}$";
	   if (id.match(regex) == null){
			alert("输入的页码只允许数字");
			return false;
	   }     
	   return true; 
    }
	function goSearch(pagenumber,pagecount)
	{   
		var pageno=document.forms["pageform"].pageNumber.value;
		if (pageno != null){
			if(pagenumber==pageno){
				return false;
			}
				
			if (pageno >0){
	            if (pageno > pagecount)         
			       document.getElementById('pageNumber').value=pagecount;
	           	
			}else if (pageno <1){
			    document.getElementById('pageNumber').value=1;
			}
		 if(!checkpageNo(pageno)){ 
          	return false;
       	 }     	 
	  }
	}

	</script>
	</head>

	<body>
		

		<form id="myform" name="myform" action="/admin/tocreateadmin" style="width:100%;">
		
			<table style="width:100%;align: left;">
				<tr>
				<td style="width: 100%">
				</br></br>
					&nbsp;&nbsp;<span class="title">操作</span>
					<br>
					<!-- <span class="remark">说明：本页显示用户提交的信息</span> -->
				</td>
				</tr>
				<tr>
					<td>
						<table style="width:100%">
							<tr>
								<td align="right" class="loginfontstyle" style="font-size:14px;" >
									
									&nbsp;&nbsp;&nbsp;&nbsp;
							
									&nbsp;&nbsp;<input type="submit" class="buttonstyle" value="新建" >
									<!-- &nbsp;&nbsp;<input type="button" class="buttonstyle"  value="导出本页" onclick="exportNowPage()"> -->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="30px">
				</tr>
				<tr>
					<td  background="images/hr_1.gif"
						style="height: 1px; width: 100%">
						<div class="hr1">
						</div>
					</td>
				</tr>
				<tr>
				<td style="width: 100%">
					<span class="title">用户信息表</span>
					<br>
				</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="3" cellspacing="0"
							bordercolor="#AAAAAA" border="1"
							style="border-collapse: collapse" align="left">
							<tr BGCOLOR="#1E90FF" align="center" style="height: 30px">
							
								<td class="formtitlestyle" style="width: 20%">
									<b>用户编号</b>
								</td>
								
								<td class="formtitlestyle" style="width: 20%">
									<b>用户姓名</b>
								</td>
								<td class="formtitlestyle" style="width: 20%">
									<b>用户状态</b>
								</td>
								<td class="formtitlestyle" style="width: 20%">
									<b>用户角色</b>
								</td>
								<td class="formtitlestyle" style="width: 20%">
									<b>操作</b>
								</td>
							</tr>
								<#list page.getList() as admin >
								<tr style="height: 25px">
									<td class="loginfontstyle" style="width: 20%" align="center">
										${admin.id}
									</td>
									<td class="loginfontstyle" style="width: 20%" align="center">
										${admin.name}
									</td>
									<td class="loginfontstyle" style="width: 20%" align="center">
										<#if admin.status.equals("1")>
											正常
										<#else>
											已禁用
										</#if>
									</td>
									<td class="loginfontstyle" style="width: 20%" align="center">
										<#if admin.permission.equals("1")>
											导出角色
										<#elseif admin.permission.equals("2")>
											导入角色
										<#elseif admin.permission.equals("0")>
											管理员角色
										<#elseif admin.permission.equals("3")>
											查询角色
										<#elseif admin.permission.equals("4")>
											发货角色
										</#if>
									</td>
									<td class="loginfontstyle" style="width: 20%" align="center">
									<#if !admin.id.equals("admin") >
											<#if admin.status.equals("1")>
												<a href="/admin/hidden?adminid=${admin.id}"  >禁用</a>
											<#else>
												<a href="/admin/lunch?adminid=${admin.id }">启用</a>
											</#if>
									</#if>
											
									</td>
									
								</tr>
							</#list>
						</table>
					</td>
				</tr>
			</table>
		</form>
		
		<form id="pageform" name="pageform"  onsubmit=" return goSearch(${page.pageNumber! },${page.totalPage!});">
			<table width="100%" border="0" align="center" cellpadding="0"
			cellspacing="0">
			<tr style="color: black;">
				<td height="30" align="center">
					共
					<strong>${page.totalRow!}
					</strong> 条&nbsp;&nbsp;
					<#if page.getPageNumber() gt 1 >
						<a style="cursor: hand"	href="/admin/adminmanage?pageNumber=1"><font color="red"><b>[首页]</b></font></a>
						<a style="cursor: hand"	href="/admin/adminmanage?pageNumber=${page.pageNumber-1 }"><font color="red"><b>[上一页]</b></font></a>
					<#else>
						<b>[首页]</b>
						<b>[上一页]</b>
					</#if>
					
					<#if page.getPageNumber() lt page.getTotalPage() >
						<a style="cursor: hand"	href="/admin/adminmanage?pageNumber=${page.pageNumber+1}"><font color="red"><b>[下一页]</b></font></a>
						<a style="cursor: hand"	href="/admin/adminmanage?pageNumber=${page.totalPage!}"><font color="red"><b>[尾页]</b></font></a>
					<#else>
						<b>[下一页]</b>
						<b>[尾页]</b>
					</#if>
					
					&nbsp;&nbsp;页次：
					${page.pageNumber!}
					/
					<#if page.totalPage==0>
						1
					<#else>
					${page.totalPage!}
					</#if>
					页
					<strong>${page.pageSize!}
					</strong>条/页 转到：第
						<input type="text" size="2" id="pageNumber" name="pageNumber"  value="${(page.pageNumber)!}" class="tfield7" />
						页
						<input type="submit" class="buttonstyle" value="go!"  />
				</td>
			</tr>
		</table>
	</form>
	</body>
</html>
