﻿create table uinfo_t(
	cardno varchar(15) primary key,
	password varchar(15) not null,
	cardcomment varchar(50),
	name varchar(15) default '',
	phoneno varchar(15) default '',
	datetime varchar(20) default '',
	address varchar(200) default '',
	used enum('否','是') default '否',
	comment varchar(255)  default '',
	delivered enum('0','1') default '0'
);

create table admin_t(
	id varchar(15) primary key,
	password varchar(200) not null,
	name varchar(20) default '',
	permission varchar(2) not null,
	status enum('1','0') default '1'
);